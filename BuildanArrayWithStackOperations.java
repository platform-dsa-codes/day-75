import java.util.ArrayList;
import java.util.List;

class Solution {
    public List<String> buildArray(int[] target, int n) {
        List<String> operations = new ArrayList<>();
        int targetIndex = 0; // Pointer to iterate through the target array
        
        // Iterate from 1 to n
        for (int i = 1; i <= n; i++) {
            if (targetIndex == target.length) {
                // If we have reached the end of the target array, no need to continue
                break;
            }
            
            if (target[targetIndex] == i) {
                // If the current integer matches the next target element, push it to the stack
                operations.add("Push");
                targetIndex++;
            } else {
                // If not, push and pop to maintain the order
                operations.add("Push");
                operations.add("Pop");
            }
        }
        
        return operations;
    }
}
