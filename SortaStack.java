//{ Driver Code Starts
import java.util.Scanner;
import java.util.Stack;
class SortedStack{
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int t=sc.nextInt();
		while(t-->0){
			Stack<Integer> s=new Stack<>();
			int n=sc.nextInt();
			while(n-->0)
			s.push(sc.nextInt());
			GfG g=new GfG();
			Stack<Integer> a=g.sort(s);
			while(!a.empty()){
				System.out.print(a.peek()+" ");
				a.pop();
			}
			System.out.println();
		}
	}
}
// } Driver Code Ends


class GfG {
    public Stack<Integer> sort(Stack<Integer> s) {
        if (!s.isEmpty()) {
            // Remove the top element
            int temp = s.pop();
            
            // Recursively sort the remaining stack
            sort(s);
            
            // Insert the popped element in sorted order
            insertInSortedOrder(s, temp);
        }
        return s;
    }
    
    // Helper function to insert element in sorted order
    private void insertInSortedOrder(Stack<Integer> s, int temp) {
        // If stack is empty or top element is smaller than temp, push temp
        if (s.isEmpty() || s.peek() < temp) {
            s.push(temp);
            return;
        }
        
        // If top element is greater than temp, remove top element and call insertInSortedOrder recursively
        int top = s.pop();
        insertInSortedOrder(s, temp);
        
        // Push the removed element back to stack
        s.push(top);
    }
}
